#!/usr/bin/env bash

# Define file paths
CI_TEMPLATE="ci-templates/benchmark.yml"
SUBMODULE="benchmark"

# If there are no staged changes to the submodule or template, there's nothing to do
if git diff --quiet --cached "$SUBMODULE" "$CI_TEMPLATE"; then
  exit 0
fi

# Get the current staged submodule commit hash
SUBMODULE_REF=$(git ls-files --format='%(objectname)' $SUBMODULE)

# Get the ref field from ci-templates/benchmark.yml
CI_REF=$(sed -En "s/.*ref: +[\"']?([0-9a-fA-F]{40})[\"']? +# must match benchmark submodule hash.*/\1/p" $CI_TEMPLATE)
if [ -z "$CI_REF" ]; then
  echo "Failed to find CI_REF in $CI_TEMPLATE"
  exit 1
fi

# Check and update the ref field in ci-templates/benchmark.yml
if [ "$SUBMODULE_REF" == "$CI_REF" ]; then
  echo "$SUBMODULE ref is up to date. (ref: $CI_REF)"
else
  echo "$SUBMODULE ref has changed. Updating ref from $CI_REF to $SUBMODULE_REF."
  sed -i '' "s/ref: .*$CI_REF.* # must match benchmark submodule hash.*/ref: '$SUBMODULE_REF' # must match benchmark submodule hash, see docs\/benchmark.md/g" "$CI_TEMPLATE"
  git add "$CI_TEMPLATE"
  echo "$SUBMODULE updated and commit amended. (new ref: $SUBMODULE_REF)"
fi
