#!/usr/bin/env bash

# Define file paths
RULES="sast-rules"
SERVICE="sast-scanner-service"

# If there are no staged changes to either submodule, there's nothing to do
if git diff --quiet --cached "$RULES" "$SERVICE"; then
  exit 0
fi

# Get the current staged submodule commit hash
SUBMODULE_REF=$(git ls-files --format='%(objectname)' $SUBMODULE)

# Get the configured SAST_RULES_VERSION
SAST_RULES_VERSION=$(sed -En "s/.*SAST_RULES_VERSION: [\"']?([0-9.]*)[\";]?.*/\1/p" "$SERVICE/.gitlab-ci.yml")
if [ -z "$SAST_RULES_VERSION" ]; then
  echo "Failed to find SAST_RULES_VERSION in $SERVICE/.gitlab-ci.yml"
  exit 1
fi

# Get the tag from the RULES submodule
RULES_TAG=$(git -C "$RULES" describe --tags --abbrev=0)
RULES_TAG=${RULES_TAG#v}

if [ -z "$RULES_TAG" ]; then
  echo "Failed to get tag from $RULES submodule"
  exit 1
fi

# Compare SAST_RULES_VERSION with RULES_TAG
if [ "$SAST_RULES_VERSION" != "$RULES_TAG" ]; then
  echo "SAST_RULES_VERSION ($SAST_RULES_VERSION) does not match the latest tag in $RULES submodule ($RULES_TAG)"
  echo "Please update SAST_RULES_VERSION in $SERVICE/.gitlab-ci.yml"
  exit 1
fi

echo "SAST_RULES_VERSION matches the latest tag in $RULES submodule ($RULES_TAG)"