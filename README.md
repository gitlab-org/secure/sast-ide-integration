# SAST IDE integration

This project brings together the components of GitLab's "SAST in the IDE" solution for the purposes of development, benchmarking, and deployment.

## Components

- [scanner service](https://gitlab.com/gitlab-org/secure/sast-scanner-service) - a [Cloud Connector](https://docs.gitlab.com/ee/development/cloud_connector/architecture.html)
  backend service deployed with [Runway](https://handbook.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/), see [`.runway/`](./.runway).
- [`sast-rules`](https://gitlab.com/gitlab-org/security-products/sast-rules) - the ruleset deployed with the scanner service. (not currently enforced)
- IDE components: [LSP language server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp) and [GitLab Workflow extension for VS Code](https://gitlab.com/gitlab-org/gitlab-vscode-extension).
- [benchmarking suite](https://gitlab.com/gitlab-org/secure/sast-ide-benchmark) for evaluating service performance.

## Setup

Note: The development scripts are currently `zsh`.

Run

```sh
./scripts/setup.sh
```

The [setup script](scripts/setup.sh):

1. uses [`asdf`](https://asdf-vm.com) or [`mise`](https://mise.jdx.dev/) to install `go`, `python`, and `ruby`. If a plugin is missing, a prompt will ask to install it.
2. [checks out submodules](./docs/submodules.md#checkout-submodules-at-their-currently-referenced-commits).
3. creates a Python virtual environment and installs `semgrep` with `pip`
4. runs `asdf install` (or `mise install`) and `npm ci` in the `gitlab-lsp` and `gitlab-vscode-extension` submodules
5. uses `go` to install the `Git` hook manager [`lefthook`](https://github.com/evilmartians/lefthook), and sets it up.

This covers the prerequisites of each component:

- The `sast-scanner-service` is written in `go`, but relies on `semgrep` which is distributed via `python`.
- The `sast-rules` project distribution of `semgrep` rules is [built](scripts/build-rules.sh) with a `ruby` script.
- Both `gitlab-lsp` and `gitlab-vscode-extension` projects manage their requirements in `.tool-versions` files.
