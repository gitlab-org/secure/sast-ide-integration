#!/bin/zsh

SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

cd "$BASE/gitlab-vscode-extension"

yalc remove @gitlab-org/gitlab-lsp
npm install @gitlab-org/gitlab-lsp