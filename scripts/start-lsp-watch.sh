#!/bin/zsh

SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

cd "$BASE/gitlab-lsp"
npm run watch -- --editor=vscode