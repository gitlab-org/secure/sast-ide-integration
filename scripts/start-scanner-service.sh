#!/bin/zsh

SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

. "$BASE/.venv/bin/activate"

export RULESET="$BASE/rules"
export OSEMGREP_BIN_PATH="$BASE/sast-scanner-service/osemgrep"

if [ ! -d "$RULESET" ]; then
    echo "buliding rules"
    "$SCRIPTS/build-rules.sh"
fi

if [ ! -e "$OSEMGREP_BIN_PATH" ]; then
    echo "linking $OSEMGREP_BIN_PATH to semgrep-core binary"
    ln -s "$(python -c 'import semgrep;print(semgrep.__path__[0])')/bin/semgrep-core" "$OSEMGREP_BIN_PATH"
fi

cd "$BASE/sast-scanner-service"
SECURE_LOG_LEVEL=debug go run . -- serve -c "$BASE/rules" -auth NoAuth
