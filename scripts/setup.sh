#!/bin/zsh
SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

function runtime_manager() {
    if command -v mise >/dev/null 2>&1; then
        echo "mise"
    elif command -v asdf >/dev/null 2>&1; then
        echo "asdf"
    else
        echo "No supported runtime manager found" >&2
        return 1
    fi
}

RUNTIME_MANAGER=$(runtime_manager)
if [ $? -ne 0 ]; then
    echo "Error: Neither mise nor asdf is installed. Please install one of them to continue."
    exit 1
fi

function check_plugin() {
    local tmp answer plugin="$1"

    # mise will automatically install missing plugins
    if [ "$RUNTIME_MANAGER" = "mise" ]; then
        return 0
    fi

    tmp=$($RUNTIME_MANAGER help $plugin 2>&1)
    if [ "$?" -ne 0 ] && [[ "$tmp" == "No plugin named "* ]]; then
        echo "$RUNTIME_MANAGER plugin \"$plugin\" doesn't seem to be installed."
        echo "To install, run"
        echo "  $RUNTIME_MANAGER plugin add $plugin"
        if read -q "?Run now (y/n)? "; then
            echo
            if ! $RUNTIME_MANAGER plugin add "$plugin"; then
                echo "Failed to add plugin $plugin"
                exit 1
            fi
            return 0
        else
            echo
            return 1
        fi
    fi
    return 0
}

# checked_install - check that plugins are available before running install
function checked_install() {
    local failed
    failed=0
    while read plugin version; do
        if [ "$plugin" != "#" ]; then
            if ! check_plugin "$plugin"; then
                failed=1
            fi
        fi
    done <.tool-versions

    if [ "$failed" -eq 1 ]; then
        echo "$RUNTIME_MANAGER checks failed, exiting..."
        exit 1
    fi

    $RUNTIME_MANAGER install
}

cd "$BASE"
checked_install

git submodule update --init --recursive

SCANNER_VERSION=$(sed -n -e 's/.*SCANNER_VERSION: "\(.*\)".*/\1/p' "$BASE/sast-scanner-service/.gitlab-ci.yml")

if [ -z "$SCANNER_VERSION" ]; then
    echo "SCANNER_VERSION not found. Check sast-scanner-service/.gitlab-ci.yml"
    exit 1
fi

if [ ! -d "$BASE/.venv" ]; then
    python -mvenv .venv
fi

. "$BASE/.venv/bin/activate"

pip3 install semgrep==$SCANNER_VERSION

for proj in gitlab-lsp gitlab-vscode-extension; do
    cd "$BASE/$proj"
    checked_install
    export NODE_GYP_FORCE_PYTHON=$($RUNTIME_MANAGER which python)
    $RUNTIME_MANAGER install
    npm ci
done
