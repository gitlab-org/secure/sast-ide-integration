# Debugging

Runtime components can be debugged seperately or in tandem:

1. The scanner service, a golang application, can be run locally and debugged with `dlv`.
2. The GitLab Workflow VSCode extension can be debugged with VSCode using the extension host launch configuration in `gitlab-vscode-extension`.
3. The language server, a Node.js process, can be debugged in VSCode with the launch configurations in `gitlab-lsp`, either by starting it directly or attaching to the language server launched by the VSCode extension.

## Scanner service

The scanner service is implemented in golang but relies on the `semgrep` command line tool that is distributed as a pip package.

Use [`/scripts/setup.sh`](/scripts/setup.sh) to install prerequisites.

### Starting the scanner service locally

The script [`/scripts/start-scanner-service.sh`](/scripts/start-scanner-service.sh) builds the ruleset and prepares `semgrep` if necessary then starts the scanner service in `NoAuth` mode so that no authentication is required.

When [used through GDK](#testing-the-scanner-service-with-gdk), the scanner service can also be run in `GitLabOIDC` mode which leverages the GitLab API and Cloud Connector for authentication and authorization e.g.

```sh
go run . -- serve -c ../rules --auth GitLabOIDC --oidc-provider http://localhost:3000
```

ctrl-c to stop the server.

## Testing the scanner service with `curl`

1. [Start the scanner service](#starting-the-scanner-service-locally)
1. Run

   ```sh
   curl \
    -qs -H "Content-Type: application/json" \
    -d '{
     "file_path":"src/main.c",
     "content":"#include<string.h>\nint main(int argc, char **argv) {\n  char buff[128];\n  strcpy(buff, argv[1]);\n  return 0;\n}\n"
    }' \
    "http://localhost:8080/scan"
   ```

   to produce JSON output containing a vulnerability.

### Testing the scanner service with `GDK`

The GitLab API [security scan endpoint](https://docs.gitlab.com/ee/api/projects.html#real-time-security-scan) used by the language server can be setup locally using `GDK`.

1. [Start the scanner service](#starting-the-scanner-service-locally)
1. [Install GDK locally](https://gitlab.com/gitlab-org/gitlab-development-kit#local)
1. Configure GDK in SaaS mode and set the scanner service URL with environment variables, e.g. in `gdk/env.runit`

   ```sh
   export GITLAB_SIMULATE_SAAS=1
   export SCANNER_SERVICE_URL=http://localhost:8080
   ```

   and restart GDK.

1. The endpoint is an Ultimate only feature, so GDK must be configured with a license.

   As `root`, navigate to Admin->Settings->General->Add License and upload your license file (GitLab team members can [request a license](https://handbook.gitlab.com/handbook/support/readiness/operations/docs/policies/team_member_licenses/)).

1. Choose a personal access token (PAT)

   E.g. to create a new token, navigate to <http://localhost:3000/-/user_settings/personal_access_tokens?name=My+GDK+token&scopes=api>, click `Create personal access token`, copy to clipboard and

   ```sh
   read -s MY_GDK_TOKEN
   ```

   and paste.

1. Run

   ```sh
   curl -H "PRIVATE-TOKEN: $MY_GDK_TOKEN" \
    -qs -H "Content-Type: application/json" \
    -d '{
     "file_path":"src/main.c",
     "content":"#include<string.h>\nint main(int argc, char **argv) {\n  char buff[128];\n  strcpy(buff, argv[1]);\n  return 0;\n}\n"
    }' \
    "http://localhost:3000/api/v4/projects/7/security_scans/sast/scan"
   ```

   to produce JSON output containing a vulnerability.

## setup a test project for use with GDK

Evaluating the extension requires a test project with some local settings.

1. [Start GDK](#testing-the-scanner-service-with-gdk)
1. The extension will use the git remote of the project to identify the instance to use for API calls.

   To pair a project with our local GDK instance, check out a project from our local GDK instance:

   ```sh
   git clone http://127.0.0.1:3000/flightjs/Flight.git testproject
   ```

1. Copy some vulnerable code into the project directory:

   ```sh
   mkdir testproject/src
   echo  "content":"#include<string.h>\nint main(int argc, char **argv) {\n  char buff[128];\n  strcpy(buff, argv[1]);\n  return 0;\n}\n" > testproject/src/main.c
   ```

1. Configure the settings.

   Press shift-cmd-p, type "Preferences: Open Workspace Settings (JSON)", then press enter.

   Alternatively, edit the file `testproject/.vscode/settings.json` directly.

   Add the following:

   ```json
     // for more verbose logging
     "gitlab.debug": true,
   
     // to enable security scans
     "gitlab.featureFlags.remoteSecurityScans": true,
     "gitlab.securityScans.enabled": true,
   ```

## GitLab Workflow Extension

Debugging a VSCode extension can be confusing, because VSCode is debugging VSCode. The extension project is active in the main VSCode window, and we debug a second VSCode window where the extension runs.

See the [official documentation](https://code.visualstudio.com/api/get-started/your-first-extension#debugging-the-extension) for a simpler example.

1. Open the extension project in VSCode

   ```sh
   code  ./gitlab-vscode-extension
   ```

1. [Select and run](#debugging-in-vscode) the `Run Extension` launch configuration. A new window with title beginning `[Extension Development Host]` will open. Note that scans should be initiated from the `[Extension Development Host]` and not from the main VSCode window.
1. In the extension development host window, navigate to your [test project](#setup-a-test-project-for-use-with-gdk).
1. If you haven't already authenticated with your GDK instance, do that as follows:
   1. shift-cmd-p, type `Gitlab: Authenticate`, press enter
   1. select `Manually enter instance URL`
   1. enter `http://127.0.0.1:3000` - Note the schema is `http:` and not `https:`!
   1. either create or enter an existing token
1. To trigger a scan, either save a file or press shift-cmd-p, type "GitLab: Run Security Scan", and hit enter.
1. After a delay, findings will be listed in the "Problems" tab and underlined in the editor.
1. To see `log` messages from the extension, open the output window in the extension host window - press shift-cmd-u and select "GitLab Workflow" from the drop down.

Breakpoints can be set in `gitlab-vscode-extension` code in the main VSCode window.

## GitLab language server

To start the language server process for debugging, pass the flag `--inspect=6010`. This can be done w/ the `debug` run script in the `gitlab-lsp` project, e.g.

```sh
npm run debug
```

Language server is usually launched by the GitLab Workflow VSCode extension, so it is probably more useful to attach to an already running language server.

1. In order to synchronize `gitlab-vscode-extension` with source dependencies on `gitlab-lsp`, the package loaded by the extension must coincide with the code being debugged.

   There is an `npm` script called `watch` in `gitlab-lsp` that synchronized projects and reloads when code changes. [`/scripts/start-lsp-watch.sh`](/scripts/start-lsp-watch.sh) runs watch from the correct directory with typical arguments.

1. Start the watch script with

   ```sh
   ./scripts/start-lsp-watch.sh
   ```

   When done debugging, ctrl-c to exit and run

   ```sh
   ./scripts/stop-lsp-watch.sh
   ```

   to cleanup the package dependency injected into `gitlab-vscode-extension`

1. [Launch the extension host](#gitlab-workflow-extension) from the `gitlab-vscode-extension` project and open the [test project](#setup-a-test-project-for-use-with-gdk) in the "Extension Development Host" window

1. In the main VSCode instance or a new instance (not the Extension Development Host window!), open the language server project

   ```sh
   code  ./gitlab-lsp
   ```

1. [Select and run](#debugging-in-vscode) the `Attach to VS Code Extension` launch configuration.

## debugging in VSCode

VSCode supports different debugger launch configurations.  Open `Run and Debug` from the side bar by pressing shift-cmd-d.  The launch configurations are listed in the dropdown to the right of the `RUN AND DEBUG` label at the top of the sidebar.

Once selected, pressing the "play" button or F5 will start the debugging session.
