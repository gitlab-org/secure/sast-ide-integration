# using git submodules

We use [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to manage project collaborations.

## overview of the workflow

Note: You can work locally with any combination of component branches by `git switch`ing from within a submodule directory. There's no need to use `git submodule`.  E.g. from my local `sast-ide-integration` project directory, I can

```sh
git -C sast-scanner-service switch sast-ide_my-feature
git -C gitlab-vscode-extension switch sast-ide_my-feature
```

To share changes, create a branch in `sast-ide-integration` that [sets submodules branches](#set-submodule-branches-for-collaborating-branches-of-component-projects). This makes it easy to [stay up to date across components](#update-submodules-to-the-head-of-their-set-branches).

When changes are file, the merge process is as follows:

1. Merge component changes
   - `sast-scanner-service/sast-ide_my-feature` into `sast-scanner-service/main` and
   - `gitlab-vscode-extension/sast-ide_my-feature` into `gitlab-vscode-extension/main`
1. Update submodules references and, if necessary, reset submodule branches to default

   ```sh
   git switch my-feature

   git submodule set-branch --default sast-scanner-service
   git submodule set-branch --default gitlab-vscode-extension
   git submodule update --init --recursive --remote --rebase sast-scanner-service gitlab-vscode-extension
   
   git add sast-scanner-service gitlab-vscode-extension && git commit -m 'Add my feature' 
   ```

   NOTE:

     The `main` branch of `sast-ide-integration` must be [deployable](./runway.md#deploying-a-service-image), meaning that [production images](https://gitlab.com/gitlab-org/secure/sast-scanner-service/container_registry/6793804) have been created for the pinned commit. Production images can be created in two ways

     1. by [tagging](https://gitlab.com/gitlab-org/secure/sast-scanner-service/-/tags) a commit or
     1. by naming a branch `v###` and running its pipeline with `PRODUCE_IMAGES=true`.

1. Create the merge request  `sast-ide-integration/my-feature` -> `sast-ide-integration/main`

## checkout submodules at their currently referenced commits

When this repository is first checked out, the submodule directories must be populated, e.g.

```sh
git submodule update --init --recursive
```

## update submodules to the HEAD of their set branches

To pull the latest from each submodule's set branch, run the following:

```sh
git submodule update --init --recursive --remote --rebase
```

Note, the `--rebase` flag is only necessary if a submodule is tracking a branch.  Without `--rebase`, any local commits will be ignored and the submodule will go into detached HEAD state at the remote branch HEAD.

If a submodule has changed, `git status` will show "new commits" for the submodule.  To stage changes to a submodule, `git add` its path, e.g.

```sh
git submodule add sast-rules
git submodule add sast-scanner-service
git submodule add gitlab-vscode-extension
```

## set submodule branches for collaborating branches of component projects

Assuming that each component project has a branch `sast-ide_my-feature`, create a branch in this repo to use them together, e.g.

```sh
git switch -c my-feature
git submodule set-branch -b sast-ide_my-feature sast-scanner-service
git submodule set-branch -b sast-ide_my-feature gitlab-vscode-extension
# update to the HEAD of each remote submodule branch
git submodule update --init --recursive --remote --rebase
# stage changes to the submodules configuration
git add .gitmodules
git add sast-scanner-service
git add gitlab-vscode-extension
```

## troubleshooting

- Read the [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) chapter in the Pro Git book.
- Work out rebasing issues within each submodule directory then re-run `git submodules update`.
- If a submodule is in detached HEAD state, you can `git switch` to its set branch.
