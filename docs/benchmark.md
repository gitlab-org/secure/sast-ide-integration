# Benchmark

[sast-ide-benchmark](https://gitlab.com/gitlab-org/secure/sast-ide-benchmark) is a component of [`Static Analysis: real-time IDE SAST`](https://gitlab.com/groups/gitlab-org/-/epics/13753),
which provides a benchmarking framework and test cases.

## Implementation

The benchmarking suite assumes that the scanner service is provided in the form of a docker image.
The version of the image is pinned to that of the submodule `sast-scanner-service` in this `sast-ide-integration` repo. 
Its URL is inferred via `registry.gitlab.com/gitlab-org/secure/sast-scanner-service/tmp:$(git ls-tree HEAD --object-only ./sast-scanner-service)-service`
The [`ref` used to include benchmark CI templates](/ci-templates/benchmark.yml#L23) 
must match the commit hash of the [benchmark submodule](/.gitmodules#L13) in order to
ensure correct benchmark pipeline configuration.

Using this image, both local and remote servers are allocated automatically in testing.
A local server is allocated via [`service`](https://docs.gitlab.com/ee/ci/services/).
Remote `GCP` servers (with firewall policies) are allocated in five locations around the world to test network latency.
Remote resources are deallocated when the corresponding testing job finishes. 

The scanning reports are produced in each job in the `run-benchmark` as downloadable artifacts.

After all the tests are done, the metrics, including the sizes of programs scanned and scanning time consumed, 
are produced also as an `html` artifact both downloadable and viewable with a web browser. 

### Remote servers

Remote servers are used to test network latency in different locations. 
They are automatically allocated in `GCP` at the start of the test 
and deallocated at the end to save costs, requiring no user configuration.

The `Compute Engine default service account` under the group-secure project manages `GCP` resources, 
with its token configured as a protected `CICD` variable. 
If resource management fails, please contact the maintainers.

## How to use

To run, just include `ci-templates/benchmark.yml` in
[.gitlab-ci.yml](/.gitlab-ci.yml#L15)

To configure, go to [`ci-templates/benchmark.yml`](/ci-templates/benchmark.yml),
where you can specify [how many test programs you want to use](/ci-templates/benchmark.yml#L3), 
and [from which specific directory](/ci-templates/benchmark.yml#L4)

To update the `benchmark` submodule to a specific commit:
```
cd benchmark && git checkout "THE_COMMIT_HASH_YOU_LIKE"
cd - && git add benchmark
```
Note however, you must manually update the `ref` in [`ci-templates/benchmark.yml`](/ci-templates/benchmark.yml#L23)
as well before `git push`, to ensure submodule consistency, or the `check_ref` job in the pipeline will fail.

(A `lefthook` client-side plugin for this is in development to assist automation and will be released later.)

For more details about the use of git submodules, please refer to [`submodules.md`](/docs/submodules.md).

## GCP project setup

See the ["bootstrap" documentation]((https://gitlab.com/gitlab-org/secure/sast-ide-benchmark/-/blob/9303b825bfa1e2d8d003b854094d8a28ddcb8287/doc/bootstrap_oidc/README.md)) in the `sast-ide-benchmark` project.
